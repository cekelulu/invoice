<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Update Customer Invoice List</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
<?PHP
include ('001link_db2.php'); 
$CustomerNo=$_GET['CustomerNo'];
$query="SELECT * FROM customerinvoices WHERE CustomerNo='$CustomerNo'";
$result=mysqli_query($conn,$query);
$num= mysqli_num_rows($result);

$i=0;
while ($i < $num) {
$show=mysqli_fetch_assoc($result);
$CustomerNo=$show['CustomerNo'];
$CustomerName=$show['CustomerName'];
$Address=$show['Address'];
$InvoiceNo=$show['InvoiceNo'];
$InvoiceDate=$show['InvoiceDate'];
$OrderNo=$show['OrderNo'];
$TotalAmount=$show['TotalAmount'];
$Discount=$show['Discount'];
$AmountDue=$show['AmountDue'];

?>
<div class="container">
<form action="011pros_update.php" method="post">
  <div class="form-group">
    <label for="CustomerNo">Customer No:</label>
    <input type="text" class="form-control" name="ud_CustomerNo" id="ud_CustomerNo" readonly="readonly" value="<?php echo $CustomerNo; ?>" />
  </div>
  <div class="form-group">
    <label for="CustomerName">Customer Name:</label>
    <input type="text" class="form-control" name="ud_CustomerName" id="ud_CustomerName"  value="<?php echo $CustomerName; ?>" />
  </div>
  
  <div class="form-group">
    <label for="Address">Address:</label>
    <input type="text" class="form-control" name="ud_Address" id="ud_Address"  value="<?php echo $Address; ?>" />
  </div>
  
  <div class="form-group">
    <label for="InvoiceNo">Invoice No:</label>
    <input type="text" class="form-control" name="ud_InvoiceNo" id="ud_InvoiceNo"  value="<?php echo $InvoiceNo; ?>" />
  </div>
  
  <div class="form-group">
    <label for="InvoiceDate">Invoice Date:</label>
    <input type="date" class="form-control" name="ud_InvoiceDate" id="ud_InvoiceDate"  value="<?php echo $InvoiceDate; ?>" />
  </div>

  <div class="form-group">
    <label for="OrderNo">Order No:</label>
    <input type="text" class="form-control" name="ud_OrderNo" id="ud_OrderNo"  value="<?php echo $OrderNo; ?>" />
  </div>
  
  <div class="form-group">
    <label for="TotalAmount">Total Amount:</label>
    <input type="float" class="form-control" name="ud_TotalAmount" id="ud_TotalAmount"  value="<?php echo $TotalAmount; ?>" />
  </div>
  
  <div class="form-group">
    <label for="Discount">Discount:</label>
    <input type="float" class="form-control" name="ud_Discount" id="ud_Discount"  value="<?php echo $Discount; ?>" />
  </div>
  
    <div class="form-group">
    <label for="AmountDue">Amount Due:</label>
    <input type="float" class="form-control" name="ud_AmountDue" id="ud_AmountDue"  value="<?php echo $AmountDue; ?>" />
  </div>
  
  <button  name="SUBMIT" type="SUBMIT" class="btn btn-default">SUBMIT</button>
</form>

<?PHP
++$i;
}
?>
</body>
</html>