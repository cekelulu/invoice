<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Update Shipping Packages</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
<?PHP
include ('001link_db2.php'); 
$ID=$_GET['ID'];
$query="SELECT * FROM shippingpackages WHERE ID='$ID'";
$result=mysqli_query($conn,$query);
$num= mysqli_num_rows($result);

$i=0;
while ($i < $num) {
$show=mysqli_fetch_assoc($result);
$ID=$show['ID'];
$QtyOrder=$show['QtyOrder'];
$QtyShip=$show['QtyShip'];
$QtyBookOrder=$show['QtyBookOrder'];
$TotalPrice=$show['TotalPrice'];
$CustomerNo=$show['CustomerNo'];
$ProductNo=$show['ProductNo'];

?>
<div class="container">
<form action="011pros_update (2).php" method="post">
  <div class="form-group">
    <label for="ID">ID:</label>
    <input type="number" class="form-control" name="ud_ID" id="ud_ID" readonly="readonly" value="<?php echo $ID; ?>" />
  </div>
  <div class="form-group">
    <label for="QtyOrder">Quantity Order:</label>
    <input type="number" class="form-control" name="ud_QtyOrder" id="ud_QtyOrder"  value="<?php echo $QtyOrder; ?>" />
  </div>
  
  <div class="form-group">
    <label for="QtyShip">Quantity Ship:</label>
    <input type="number" class="form-control" name="ud_QtyShip" id="ud_QtyShip"  value="<?php echo $QtyShip; ?>" />
  </div>
  
  <div class="form-group">
    <label for="QtyBookOrder">Quantity Booked Order:</label>
    <input type="number" class="form-control" name="ud_QtyBookOrder" id="ud_QtyBookOrder"  value="<?php echo $QtyBookOrder; ?>" />
  </div>
  
 <div class="form-group">
    <label for="TotalPrice">Total Price:</label>
    <input type="float" class="form-control" name="ud_TotalPrice" id="ud_TotalPrice"  value="<?php echo $TotalPrice; ?>" />
  </div>
  
 <div class="form-group">
    <label for="CustomerNo">Customer No:</label>
    <input type="text" class="form-control" name="ud_CustomerNo" id="ud_CustomerNo" readonly="readonly" value="<?php echo $CustomerNo; ?>" />
  </div>
  
 <div class="form-group">
    <label for="ProductNo">Product No:</label>
    <input type="text" class="form-control" name="ud_ProductNo" id="ud_ProductNo" readonly="readonly" value="<?php echo $ProductNo; ?>" />
  </div>
  

  <button  name="SUBMIT" type="SUBMIT" class="btn btn-default">SUBMIT</button>
</form>

<?PHP
++$i;
}
?>
</body>
</html>